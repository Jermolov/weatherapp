CREATE OR REPLACE FUNCTION trigger_maxrow()
RETURN TRIGGER AS

$body$
  BEGIN
    IF(SELECT count (id)FROM weather)>10
    THEN
      DELETE FROM weather
      WHERE id = (SELECT min (id)FROM weather);
    END IF;
  END;
$body$

LANGUAGE plpgsql;
CREATE TRIGGER trigger_maxrow
AFTER INSERT ON weather
FOR EACH ROW EXECUTE trigger_maxrow();