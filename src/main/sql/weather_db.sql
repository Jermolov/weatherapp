DROP TABLE IF EXISTS weather CASCADE;

CREATE TABLE weather (
  id BIGSERIAL PRIMARY KEY,
  hourPresision TIMESTAMP NOT NULL,
  temperature INT,
  precipitation INT
);