package com.example.weatherapp.Entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class HourWeathersDto {
    @JsonProperty("data")
    List<HourWeatherDto> weatherList;
}
