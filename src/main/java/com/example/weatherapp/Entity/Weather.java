package com.example.weatherapp.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "weather")
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Weather {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "hourPresision")
    @CreationTimestamp
    private LocalDateTime hourPresision;

    @Column(name = "temperature")
    private Integer temperature;

    @Column(name = "precipitation")
    private Integer precipitation;
}
