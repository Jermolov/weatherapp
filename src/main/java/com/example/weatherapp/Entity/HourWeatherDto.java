package com.example.weatherapp.Entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class HourWeatherDto {

    @JsonProperty("temp")
    public int temperature;

    @JsonProperty("precip")
    public int precipitation;

    @JsonProperty("timestamp_uts")
    public String hourPresision;
}
