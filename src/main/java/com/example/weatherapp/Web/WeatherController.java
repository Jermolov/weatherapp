package com.example.weatherapp.Web;

import com.example.weatherapp.Entity.Weather;
import com.example.weatherapp.Persistence.WeatherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "http://localhost:8081")
@RestController
@RequestMapping("/api")
public class WeatherController {
    @Autowired
    WeatherRepository weatherRepository;

    @GetMapping("/week")
    public List<Weather> getAllWeathers(){
        List<Weather>list = new ArrayList<>();
        Iterable<Weather>weekWeather = weatherRepository.findAll();
        weekWeather.forEach(list::add);
        return list;
    }

    @GetMapping("/week/{id}")
    public ResponseEntity<Weather> getDayWeather(@PathVariable("id")long id){
        Optional<Weather> weatherData = weatherRepository.findById(id);
        if (weatherData.isPresent()){
            return new ResponseEntity<>(weatherData.get(), HttpStatus.OK);
        }else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}