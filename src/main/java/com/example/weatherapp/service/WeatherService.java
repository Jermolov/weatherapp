package com.example.weatherapp.service;

import com.example.weatherapp.Entity.HourWeatherDto;
import com.example.weatherapp.Entity.Weather;
import com.example.weatherapp.Persistence.WeatherRepository;
import com.google.common.collect.ImmutableList;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toList;

@Service
public class WeatherService{
    @Autowired
    private WeatherRepository weatherRepository;

    @Autowired
    private WeatherClient weatherClient;

    public List<Weather> findAll(){
        return weatherClient.getSites().stream()
                .map(this::toWeather)
                .collect(collectingAndThen(toList(), ImmutableList::copyOf));
    }

    private Weather toWeather(@NonNull HourWeatherDto input){
        return new Weather(
                input.getPrecipitation(),
                input.getTemperature());
    }
}
