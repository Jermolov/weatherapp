package com.example.weatherapp.service;

import com.example.weatherapp.Entity.HourWeatherDto;
import com.example.weatherapp.Entity.HourWeathersDto;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@Component
public class WeatherClient {
    private RestTemplate restTemplate = new RestTemplate();

    public List<HourWeatherDto> getSites() {
        String url ="https://api.weatherbit.io/v2.0/history/hourly?city=Helsinki, FIN&start_date=2019-01-04&end_date=2019-01-05&tz=local&key=0d03b143def7413dac91ee1deee1b20a";
        try {
            HourWeathersDto response = restTemplate.getForObject(new URI(url),HourWeathersDto.class);
            return response.getWeatherList();
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }
}
