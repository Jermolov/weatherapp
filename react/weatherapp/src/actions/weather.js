import axios from '../axios/axios';

const _getWeather = (weather)=>({
    type:'GET_WEATHER',
    weather
});

export const getWeather = () =>{
    return (dispatch) =>{
        return axios.get('week').then(result =>{
            const weather = [];
            result.data.forEach(item=>{
                weather.push(item);
            });
            dispatch(_getWeather(weather));
        })
    }
};