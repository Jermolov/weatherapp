import React from "react";
import {connect} from 'react-redux';
import TableRow from "./TableRow";

const Table = (props) =>(
    <div>
        <table>
            <caption>Some Text</caption>
            <tbody>
            <tr>
                <th>Temperature</th>
                <th>Percipitation</th>
            </tr>
                {props.weather.map(item=>{
                    return(
                        <tr>
                            <td key={item.id}>
                                <TableRow{...item}/>
                            </td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
    </div>
);

const mapStateToProps = (state) =>{
    return {
        weather:state
    };
};

export default connect(mapStateToProps)(Table);