import React from 'react';

const TodayPage =()=>(
    <div>
        <p>Weather Today</p>
        /*add chart component*/
        <table>
            <caption>Weather Now</caption>
            <tbody>
                <tr>
                    <th>Temperature</th>
                    <th>Percipitation</th>
                    <th>Time</th>
                </tr>
                <tr>
                    /*Add weather for today*/
                    <td>10</td>
                    <td>10</td>
                    <td>Today</td>
                </tr>
            </tbody>
            </table>
            /*opening modal window*/
            /*Just for testing added table for now*/
            <button>Last 24H</button>
    </div>
);

export default TodayPage;