import React from 'react';
import Modal from 'react-modal';
import Table from "./Table";

const DayModal =(props)=>(
    <Modal
        isOpen={!!props.selectedDay}
        contentLabel="Selected Day"
        onRequestClose={props.handleCloseSelectedDay}
    >
        <h1>Wether THIS day</h1>
        <Table/>
        <button onClick={props.handleCloseSelectedDay}>Close</button>
    </Modal>
);