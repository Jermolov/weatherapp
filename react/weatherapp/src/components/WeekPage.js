import React from 'react';

class WeekPage extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            weekDays:["Monday","Tuesday","Thursday","Wednesday","Friday",
                "Saturday","Sunday"],
            selectedDay: undefined
        };
        this.handleOpenDay = this.handleOpenDay.bind(this);
        this.handleCloseSelectedDay = this.handleCloseSelectedDay.bind(this);
    }
    handleOpenDay = () =>{
        const day = this.state.weekDays[1];
        this.setState (()=>({
            selectedDay:day
        }));
    };

    handleCloseSelectedDay = () =>{
        this.state(()=>({selectedDay: undefined}));
    };

    render(){
        return(
            <div>
                {this.state.weekDays.map((item)=>(
                    <button
                        className="big-button"
                        key={item}
                        onClick={this.handleOpenDay}
                    >{item}</button>
                ))}
            </div>
        )
    }
}

export default WeekPage;