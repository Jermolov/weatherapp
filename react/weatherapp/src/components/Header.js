import React from 'react';
import {NavLink} from "react-router-dom";


const Header = () =>(
    <div className="header">
        <div className="container">
            <h2>Header Text</h2>
            <div>
                <h1 className="header__title">Weather App</h1>
                <NavLink to='/' exact={true} className="header__subtitle">Today</NavLink>
                <NavLink to='/week' className="header__subtitle">Last Week</NavLink>
                <NavLink to='/month' className="header__subtitle">Last Month</NavLink>
            </div>
        </div>
    </div>
);

export default Header;