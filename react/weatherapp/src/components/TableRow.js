import React from 'react';
import {connect} from 'react-redux';

const TableRow = ({temperature, precipitation,hourPresision})=>(
    <div>
        <th>{temperature}</th>
        <th>{precipitation}</th>
        <th>{hourPresision}</th>
    </div>
);

export default connect ()(TableRow);