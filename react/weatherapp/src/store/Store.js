import {createStore, applyMiddleware} from 'redux';
import weather from '../reducers/weather';
import thunk from 'redux-thunk';

export default () =>{
    const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
    const store = createStore(weather,composeEnhancers(applyMiddleware(thunk)));
    return store;
};