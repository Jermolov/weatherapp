import React from 'react';
import * as ReactDOM from "react-dom";
import {Provider} from 'react-redux';
import AppRouter from './routers/AppRouter'
import getAppStore from './store/Store';
import {getWeather} from "./actions/weather";
import 'normalize.css';
import './styles/styles.scss'

const store = getAppStore();

const template = (
    <Provider store={store}>
        <AppRouter/>
    </Provider>
);

store.dispatch(getWeather()).then(()=>{
    ReactDOM.render(template,document.getElementById('app'));
});