const weatherReducerDefaultState = [];

export default (state = weatherReducerDefaultState, action) => {
    switch (action.type) {
        case 'GET_WEATHER':
            return action.weather;
        default:
            return state;
    }
};