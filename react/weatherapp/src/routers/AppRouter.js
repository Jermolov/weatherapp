import React from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import Header from '../components/Header';
import TodayPage from '../components/TodayPage'
import WeekPage from '../components/WeekPage'
import MonthPage from '../components/MonthPage'

const AppRouter =()=>(
    <BrowserRouter>
        <div>
            <Header/>
            <Switch>
                <Route path="/" component={TodayPage} exact={true}/>
                <Route path="/week" component={WeekPage}/>
                <Route path="/month" component={MonthPage}/>
            </Switch>
        </div>
    </BrowserRouter>
);

export default AppRouter;